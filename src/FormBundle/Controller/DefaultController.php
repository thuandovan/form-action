<?php

namespace FormBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FormBundle\Entity\Dataform;
use FormBundle\Entity\Formdvt;
use FormBundle\Form\FormdvtType;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

use Vich\UploaderBundle\Form\Type\VichImageType;
use AppBundle\Service\FileUploader;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;


use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;


// use Symfony\Component\Validator\Constraints\Length;
// use Symfony\Component\Validator\Constraints\NotBlank;
// use FormBundle\Validator\Constraints\Length;

use Symfony\Component\Validator\Constraint;
// use FormBundle\Validator\Constraints\ContainsAlphanumericValidator;




class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {   
        return $this->redirectToRoute('fos_user_security_login'); 

        // $manager = $this->getDoctrine()->getManager();
        // $query_main = $manager->createQuery('SELECT u.id, u.name, u.price, u.transaction, u.qty, u.status, u.description FROM FormBundle:Dataform u');
        // $datas = $query_main->getResult();
        

        // return $this->render('FormBundle:Default:index.html.twig',['datas'=>$datas]);
    }

    public function createAction(Request $request)
    {   

        $product = new Dataform();
        // $test_name = $product->getName();

        // dump($test_name);exit();


        // validation.yml return
 

        // validate input

        // $validator = Validation::createValidator();
        // $contraint = new Assert\Collection();

        //end validate

        $form_create = $this->createFormBuilder($product)
            // ->add('name', TextType::class, array('constraints'=>(array(new Length(array('min'=>5))))))
            ->add('imageFile', FileType::class, array('label' => 'UPLOAD (IMAGE file)'))
            ->add('name', TextType::class)
            ->add('price', MoneyType::class)
            ->add('transaction', TextType::class)
            ->add('qty', NumberType::class)
            ->add('status', ChoiceType::class, array(
                'choices' => array(
                    'have' => 1,
                    'not have' => 0,
                )   
            ))
            ->add('description', TextType::class)
            ->add('submit', SubmitType::class, array('label' => 'Create product'))
            ->getForm();
        $form_create->handleRequest($request);
        if($form_create->isSubmitted() && $form_create->isValid()){
            $name = $form_create['name']->getData();
            $price = $form_create['price']->getData();
            $transaction = $form_create['transaction']->getData();
            $qty = $form_create['qty']->getData();
            $status = $form_create['status']->getData();
            $description = $form_create['description']->getData();
            $file = $form_create['imageFile']->getData();



            //add new file 
            


            // dump($file);exit();

            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $urlFile = $this->getParameter('product_images');
            $type_file = $file->guessExtension();
            if($type_file==='jpeg')
            {
                $file->move($urlFile,$fileName);
            }else
            {

                
                dump('this type '.$type_file.' file not valid');exit();
            };

            

            // dump($urlFile);exit();
            // $urlFile = $this->container->getParameter('kernel.root_dir').'/../web/uploads/images/products';

            // dump($urlFile);exit();


            
            //end add file


            $manager = $this->getDoctrine()->getManager();
            $newProduct = new Dataform();
            $newProduct->setName($name)->setPrice($price)->setTransaction($transaction)
                ->setQty($qty)->setStatus($status)->setDescription($description)->setImage($fileName);

                

            $manager->persist($newProduct);
            $manager->flush();

            $this->addflash('notice','Added product success');
            return $this->redirectToRoute('form_homepage');
            // dump($newProduct);exit();
        }
        return $this->render('FormBundle:ActionView:create.html.twig', array('form_create' => $form_create->createView()));

    	// return $this->render('FormBundle:ActionView:create.html.twig');
    	// $manager = $this->getDoctrine()->getManager();
    	// $product = new Dataform();
    }

    public function viewAction($idProduct)
    {
        $product = $this->getDoctrine()->getRepository('FormBundle:Dataform')->find($idProduct);
        return $this->render('FormBundle:ActionView:view.html.twig',['product'=>$product]);
    }

    public function updateAction($idProduct, Request $request)
    {
        $pro_current = $this->getDoctrine()->getRepository('FormBundle:Dataform')->find($idProduct);
        // dump($pro_current);exit();
        // return $this->render('FormBundle:ActionView:update.html.twig',['product'=>$product]);
        $name_cur = $pro_current->getName();
        $price_cur = $pro_current->getPrice();
        $tran_cur = $pro_current->getTransaction();
        $qty_cur = $pro_current->getQty();
        $status_cur = $pro_current->getStatus();
        $dec_cur = $pro_current->getDescription();


        $product = new Dataform();
        $form_update = $this->createFormBuilder($product)
            ->add('name', TextType::class, array('data'=>$name_cur))
            ->add('price', MoneyType::class, array('data'=>$price_cur))
            ->add('transaction', TextType::class, array('data'=>$tran_cur))
            ->add('qty', NumberType::class, array('data'=>$qty_cur))
            ->add('status', ChoiceType::class, array(
                'data'=>$status_cur,
                'choices' => array(
                    'happi' => 1,
                    'not have' => 0,
                )
            ))
            ->add('description', TextType::class, array('data'=>$dec_cur))
            ->add('submit', SubmitType::class, array('label' => 'Update product'))
            ->getForm();
        $form_update->handleRequest($request);
        if($form_update->isSubmitted() && $form_update->isValid()){
            $name = $form_update['name']->getData();
            $price = $form_update['price']->getData();
            $transaction = $form_update['transaction']->getData();
            $qty = $form_update['qty']->getData();
            $status = $form_update['status']->getData();
            $description = $form_update['description']->getData();


            $manager = $this->getDoctrine()->getManager();
            $pro_current->setName($name)->setPrice($price)->setTransaction($transaction)
                ->setQty($qty)->setStatus($status)->setDescription($description);

            $manager->persist($pro_current);
            $manager->flush();

            $this->addflash('notice','update product success');
            return $this->redirectToRoute('form_homepage');
            // dump($newProduct);exit();
        }
        return $this->render('FormBundle:ActionView:update.html.twig', array('form_update' => $form_update->createView()));
    }

    public function deleteAction($idProduct)
    {
        $pro_current = $this->getDoctrine()
            ->getRepository('FormBundle:Dataform')->find($idProduct);
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($pro_current);
        $manager->flush();
        return $this->redirectToRoute('form_homepage');   
    }


    // public function uploadAction(Request $request)
    // {
        // $product = new Dataform();
        

        // $form_upload = $this->createFormBuilder($product)
          
        //     ->add('imageFile', FileType::class, array('label' => 'UPLOAD (IMAGE file)'))
        //     ->add('name', TextType::class)
        //     ->add('price', MoneyType::class)
        //     ->add('transaction', TextType::class)
        //     ->add('qty', NumberType::class)
        //     ->add('status', ChoiceType::class, array(
        //         'choices' => array(
        //             'have' => 1,
        //             'not have' => 0,
        //         )   
        //     ))
        //     ->add('description', TextType::class)
        //     ->add('submit', SubmitType::class, array('label' => 'Create product'))
        //     ->getForm();
        // $form_upload->handleRequest($request);

        // dump($form_upload);exit();

        // return $this->render('FormBundle:ActionView:upload.html.twig',['form_upload'=>$form_upload->createView()]);

            // FormBuilderInterface $form_upload,


            // $product = new Dataform();
            // $form_upload = $this->createFormBuilder($product);



            // dump($form_upload);exit();

        //     $form_upload
        //     ->add('translations', 'a2lix_translations',array(
        //         'required_locales' => array('bg','en')
        //     ))
        //     ->add('canvas')
        //     ->add('mode','checkbox', array('label'=> 'In sell','required'=>false))
        //     ->add('lat','text',array('label'=>'Latitude'))
        //     ->add('longt','text',array('label'=>'Longitude '))
        //     ->add('imageLeadFile', 'vich_image', array(
        //         'label'=>'Lead image Home Page (720x534)',
        //         'required' => false,
        //         'allow_delete' => true, // not mandatory, default is true
        //         'download_link' => true, // not mandatory, default is true
        //     ))
        //     ->add('imageLocationFile', 'vich_image', array(
        //         'required' => false,
        //         'allow_delete' => true, // not mandatory, default is true
        //         'download_link' => true, // not mandatory, default is true
        //     ))
        //     ->add('imagePinFile', 'vich_image', array(
        //         'required' => false,
        //         'allow_delete' => true, // not mandatory, default is true
        //         'download_link' => true, // not mandatory, default is true
        //     ))
        //     ->add('imageAligmentFile', 'vich_image', array(
        //         'required' => false,
        //         'allow_delete' => true, // not mandatory, default is true
        //         'download_link' => true, // not mandatory, default is true
        //     ))
        //     ->add('imageAligmentIconFile', 'vich_image', array(
        //         'required' => false,
        //         'allow_delete' => true, // not mandatory, default is true
        //         'download_link' => true, // not mandatory, default is true
        //     ))
        //     ->add('imageArchitectureIconFile', 'vich_image', array(
        //         'required' => false,
        //         'allow_delete' => true, // not mandatory, default is true
        //         'download_link' => true, // not mandatory, default is true
        //     ))->add('imageStageIconFile', 'vich_image', array(
        //         'required' => false,
        //         'allow_delete' => true, // not mandatory, default is true
        //         'download_link' => true, // not mandatory, default is true
        //     ))->add('imageLocationIconFile', 'vich_image', array(
        //         'required' => false,
        //         'allow_delete' => true, // not mandatory, default is true
        //         'download_link' => true, // not mandatory, default is true
        //     ))->add('imageGalleryIconFile', 'vich_image', array(
        //         'required' => false,
        //         'allow_delete' => true, // not mandatory, default is true
        //         'download_link' => true, // not mandatory, default is true
        //     ))->add('imageColumFirstFile', 'vich_image', array(
        //     'required' => false,
        //     'allow_delete' => true, // not mandatory, default is true
        //     'download_link' => true, // not mandatory, default is true
        //     ))->add('imageColumSecondFile', 'vich_image', array(
        //     'required' => false,
        //     'allow_delete' => true, // not mandatory, default is true
        //     'download_link' => true, // not mandatory, default is true
        // ))->add('imageColumThirdFile', 'vich_image', array(
        //         'required' => false,
        //         'allow_delete' => true, // not mandatory, default is true
        //         'download_link' => true, // not mandatory, default is true
        // ))->add('imageColumForthFile', 'vich_image', array(
        //         'required' => false,
        //         'allow_delete' => true, // not mandatory, default is true
        //         'download_link' => true, // not mandatory, default is true
        //     ))->getForm()
        // ;



    //     dump($form_upload);exit();

    //     $form_upload->handleRequest($request);
    //     // dump($builder);exit();
    //     return $this->render('FormBundle:ActionView:upload.html.twig',['form_upload'=>$form_upload->createView()]);
    // }

    public function uploadAction()
    {
        // C1 GET DOCTRINE -------------
        
        // $container = $this->container;
        // $doctrine = $container->get('doctrine');
        // $data = $doctrine->getRepository('FormBundle:Formdvt')->findAll();

        // END C1 DOCTRINE --------------

        $data = $this->getDoctrine()->getRepository('FormBundle:Formdvt')->findAll();



        $username = $data[0]->getUsername();
        $mail = $data[0]->getMail();
        $birth_day = $data[0]->getBirthday();
        $place_girth = $data[0]->getPlaceBirth();
        $password = $data[0]->getPassword();
        $file_name = $data[0]->getFileName();
        $file = $data[0]->getFileOj();

        // dump($data);exit();

        $user = new Formdvt();

        $username = $data[0]->getUsername();
        // $name_cur = $data->getUsername();


        $form = $this->createForm(FormdvtType::class, $user);
        $form->get('username')->setData($username);

        // ->add('username',TextType::class,array('data'=>$username));

        // $form = $this->createForm(new FormdvtType(), $user);

        // $manager = $this->getDoctrine()->getManager();
        // $data = $manager->getRepository('FormBundle:Formdvt')->findAll();

        // $data = $this->getDoctrine()->getRepository('FormBundle:Formdvt')-> findUserSortById(1);
        // dump($data);exit();
        return $this->render('FormBundle:ActionView:upload.html.twig',array('form'=>$form->createView()));
    }

    // Get data and create form, do data_form = data 


    // public function loginAction()
    // {
    //     return $this->render('@FOSUserBundle/Security/login.html.twig ');
    // }
}

