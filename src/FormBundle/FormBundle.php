<?php

namespace FormBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class FormBundle extends Bundle
{
}
