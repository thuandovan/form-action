<?php

namespace FormBundle\Entity;

/**
 * Formdvt
 */
class Formdvt
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $mail;

    /**
     * @var \DateTime
     */
    private $birthday;

    /**
     * @var string
     */
    private $placeBirth;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var \stdClass
     */
    private $fileOj;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Formdvt
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return Formdvt
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return Formdvt
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set placeBirth
     *
     * @param string $placeBirth
     *
     * @return Formdvt
     */
    public function setPlaceBirth($placeBirth)
    {
        $this->placeBirth = $placeBirth;

        return $this;
    }

    /**
     * Get placeBirth
     *
     * @return string
     */
    public function getPlaceBirth()
    {
        return $this->placeBirth;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Formdvt
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     *
     * @return Formdvt
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set fileOj
     *
     * @param \stdClass $fileOj
     *
     * @return Formdvt
     */
    public function setFileOj($fileOj)
    {
        $this->fileOj = $fileOj;

        return $this;
    }

    /**
     * Get fileOj
     *
     * @return \stdClass
     */
    public function getFileOj()
    {
        return $this->fileOj;
    }
}

